using Onestop.Navigation.Services;
using Orchard.Caching.Services;
using Orchard.DisplayManagement.Descriptors;
using Orchard.DisplayManagement.Implementation;

namespace Onestop.Navigation.ShapeTableProviders
{
    /// <summary>
    /// This shape table provider handles caching of the output of a rendered menu widget.
    /// </summary>
    public class CachedMenuShapeTableProvider : IShapeTableProvider {
        private readonly ICacheService _cache;
        public CachedMenuShapeTableProvider(ICacheService cache) {
            _cache = cache;
        }

        public void Discover(ShapeTableBuilder builder) {
            builder.Describe("Menu").OnDisplayed(OnDisplayed);
        }

        private void OnDisplayed(ShapeDisplayedContext displayed) {
            // If we got here it means that widget has been rendered anew.
            // Checking if cache key has been provided
            string key = displayed.Shape.PleaseCacheKey;
            if (key == null) return;

            // Checking if version has been provided
            int version = displayed.Shape.PleaseCacheVersion;

            // Putting the rendered content to cache under given key and with given version.
            var value = displayed.ChildContent;
            if (value != null) {
                _cache.PutVersion(key, value.ToString(), version);
            }
        }
    }
}