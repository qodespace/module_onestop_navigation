﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Orchard.UI.Navigation;
using Orchard.Utility.Extensions;

namespace Onestop.Navigation.Utilities
{
    public static class MenuItemsUtility
    {
        /// <summary>
        /// Looks a menu item up in the provided list, based on the URL.
        /// Either route data collection or string URL, or both, can be used for matching. 
        /// Route data collection is matched first.
        /// </summary>
        /// <param name="menuItems">List to search.</param>
        /// <param name="routeData">Route data collection to match to, or null.</param>
        /// <param name="targetUrl">Target URL (absolute or relative) to match, or null.</param>
        /// <param name="httpContext">Current http context. Needed for matching string URL.</param>
        /// <returns>MenuItem, if found. Null otherwise.</returns>
        public static MenuItem GetItemByUrl(IEnumerable<MenuItem> menuItems, RouteData routeData, string targetUrl, HttpContextBase httpContext) {
            if (menuItems == null) {
                return null;
            }

            foreach (var menuItem in menuItems) {
                var item = GetItemByUrl(menuItem.Items, routeData, targetUrl, httpContext);
                if (item != null) {
                    return item;
                }

                if (UrlUtility.RouteMatches(menuItem.RouteValues, routeData.Values)
                    || UrlUtility.UrlMatches(menuItem.Href, targetUrl, httpContext)) {
                    return menuItem;
                }
            }

            return null;
        }

        /// <summary>
        /// Looks a menu item up in the provided list, based on the displayed name (menu item text).
        /// </summary>
        /// <param name="menuItems">List to search.</param>
        /// <param name="name">Name to search for, case-insensitive.</param>
        /// <returns></returns>
        public static MenuItem GetItemByName(IEnumerable<MenuItem> menuItems, string name) {
            var tokens = name.ToLowerInvariant().Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            if (menuItems == null || tokens.Length == 0) {
                return null;
            }

            foreach (var menuItem in menuItems)
            {
                var decodedText = HttpUtility.HtmlDecode(menuItem.Text.Text).ToLowerInvariant().Trim();
                if (decodedText != tokens[0].Trim()) {
                    continue;
                }

                var foundItem = menuItem;
                if (tokens.Length > 0) {
                    var item = GetItemByName(menuItem.Items, String.Join("/", tokens.Skip(1)));
                    if (item != null) {
                        foundItem = item;
                    }
                }

                return foundItem;
            }

            return null;
        }

        /// <summary>
        /// Looks a menu item up in the provided list, based on the position value.
        /// </summary>
        /// <param name="items">List to search.</param>
        /// <param name="position">Position to search for.</param>
        /// <returns></returns>
        public static MenuItem GetItemByPosition(IEnumerable<MenuItem> items, string position)
        {
            if (items == null || !items.Any()) return null;

            var found = items.FirstOrDefault(i => i.Position == position);
            return found ?? items.Select(item => GetItemByPosition(item.Items, position))
                                 .FirstOrDefault(iFound => iFound != null);
        }

        /// <summary>
        /// Clears the children of all items on a given list.
        /// </summary>
        /// <param name="menuItems">List of items to clear children of.</param>
        /// <param name="excludedItems">List of items to exclude and leave children as-is.</param>
        /// <returns></returns>
        public static bool ClearChildren(IEnumerable<MenuItem> menuItems, IEnumerable<MenuItem> excludedItems = null)
        {
            var retVal = false;
            if (menuItems == null) return false;

            foreach (var menuItem in menuItems)
            {
                if (excludedItems != null && excludedItems.Contains(menuItem))
                {
                    retVal = true;
                }
                else
                {
                    var isSelected = ClearChildren(menuItem.Items, excludedItems);
                    if (!isSelected)
                    {
                        menuItem.Items = null;
                    }
                    else
                    {
                        retVal = true;
                    }
                }
            }

            return retVal;
        }

        /// <summary>
        /// Looks up a parent of a given child item in the provided list of items.
        /// </summary>
        /// <param name="menuItems">List of items to search.</param>
        /// <param name="child">Child to search parent for.</param>
        /// <param name="currentParent">Optional argument used by recursive call, please ignore.</param>
        /// <returns></returns>
        public static MenuItem GetParent(IEnumerable<MenuItem> menuItems, MenuItem child, MenuItem currentParent = null) {
            if (menuItems == null || child == null) {
                return null;
            }

            foreach (var menuItem in menuItems) {
                if (menuItem == child) {
                    return currentParent;
                }

                var item = GetParent(menuItem.Items, child, menuItem);
                if (item != null) {
                    return item;
                }
            }

            return null;
        }

        public static Stack<MenuItem> SetSelectedPath(IEnumerable<MenuItem> menuItems, RouteData currentRouteData, string targetUrl, HttpContextBase httpContext)
        {
            return SetSelectedPath(menuItems, currentRouteData.Values, targetUrl, httpContext, null);
        }

        public static Stack<MenuItem> SetSelectedPath(IEnumerable<MenuItem> menuItems, RouteValueDictionary currentRouteData, string targetUrl, HttpContextBase httpContext, Func<string, RouteValueDictionary, bool> predicate)
        {
            if (predicate == null) {
                predicate = (path, route) => false;
            }
            
            if (menuItems == null) {
                return null;
            }

            foreach (var menuItem in menuItems) {
                var selectedPath = SetSelectedPath(menuItem.Items, currentRouteData, targetUrl, httpContext, predicate);
                if (selectedPath != null) {
                    menuItem.Selected = true;
                    selectedPath.Push(menuItem);
                    return selectedPath;
                }

                if (UrlUtility.RouteMatches(menuItem.RouteValues, currentRouteData) ||
                    UrlUtility.UrlMatches(menuItem.Href, targetUrl, httpContext) || 
                    predicate(menuItem.Href, menuItem.RouteValues))
                {
                    menuItem.Selected = true;
                    selectedPath = new Stack<MenuItem>();
                    selectedPath.Push(menuItem);
                    return selectedPath;
                }
            }

            return null;
        }
    }
}