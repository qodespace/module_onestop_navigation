﻿using Orchard;

namespace Onestop.Navigation.Services
{
    public interface ISingletonLock : ISingletonDependency
    {
         
    }

    public class SingletonLock : ISingletonLock
    {
    }
}