﻿using System.Globalization;
using Orchard.Caching.Services;

namespace Onestop.Navigation.Services
{
    /// <summary>
    /// A small utility class for usage in menu caching scenarios.
    /// </summary>
    public static class CacheExtensions
    {
        private const string BaseCacheSignal = "Onestop.Navigation";

        /// <summary>
        /// Signals that a given menu was changed and cached entries need to be refreshed.
        /// It increments a stored version value for a given menu.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="differentiator">Id of a menu.</param>
        public static void SignalChange(this ICacheService cache, string differentiator)
        {
            var current = cache.GetCurrentVersion(differentiator);
            current++;

            cache.Put(GetCacheSignal(differentiator), current.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Stores a given widget output under a given key and version.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="cacheKey">Key to store the value under.</param>
        /// <param name="value">Value to store.</param>
        /// <param name="version">Version to relate this entry to.</param>
        public static void PutVersion(this ICacheService cache, string cacheKey, string value, int version)
        {
            cache.Put(cacheKey, version + ":::" + value);
            cache.FinishRefresh(cacheKey);
        }

        /// <summary>
        /// Gets current version of a given menu cache.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="differentiator">Menu id to get version for.</param>
        /// <returns>Integer that represents the current version of a given menu.</returns>
        public static int GetCurrentVersion(this ICacheService cache, string differentiator)
        {
            int currentVersion = int.TryParse(cache.Get<string>(GetCacheSignal(differentiator)), out currentVersion)
                ? currentVersion
                : 0;

            return currentVersion;
        }

        /// <summary>
        /// Gets a given cached entry for given menu.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="cacheKey">Widget output cache key</param>
        /// <param name="differentiator">Id of the related menu</param>
        /// <param name="requiresRefresh">True, if the value is stale and needs to be refreshed, false otherwise.</param>
        /// <param name="currentVersion">Current menu cache version. If entry is about to be refreshed, this should be passed to PutVersion when done rendering.
        /// </param>
        /// <returns>Cached value.</returns>
        public static string Get(this ICacheService cache, string cacheKey, string differentiator, out bool requiresRefresh, out int currentVersion)
        {
            requiresRefresh = false;

            // fetching current value
            var result = cache.Get<string>(cacheKey);

            // fetching current version
            currentVersion = GetCurrentVersion(cache, differentiator);

            if (result == null)
            {
                requiresRefresh = true;
                return null;
            }

            // fetching cached version
            var versionIdx = result.IndexOf(":::");
            int cachedVersion = versionIdx != -1 && int.TryParse(result.Substring(0, versionIdx), out cachedVersion)
                ? cachedVersion
                : 0;

            // refresh needed - check the refresh key if other node is not running refresh
            if (currentVersion != cachedVersion && !cache.IsRefreshRunning(cacheKey))
            {
                cache.StartRefresh(cacheKey);
                requiresRefresh = true;
            }

            return versionIdx != -1 ? result.Substring(versionIdx + 3) : result;
        }

        /// <summary>
        /// Gets the signal name for cache.
        /// </summary>
        /// <param name="differentiator"></param>
        /// <returns></returns>
        private static string GetCacheSignal(string differentiator) {
            return BaseCacheSignal + "." + differentiator;
        }

        /// <summary>
        /// Checks if refresh operation for a given key is running, 
        /// ie. if any other node started, but haven't yet finished the operation.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="cacheKey">Cache key to check status for.</param>
        /// <returns></returns>
        private static bool IsRefreshRunning(this ICacheService cache, string cacheKey) {
            return cache.Get<string>("R::" + cacheKey) != null;
        }

        /// <summary>
        /// Starts the refresh operation of a given cache key.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="cacheKey">Cache key to refresh.</param>
        private static void StartRefresh(this ICacheService cache, string cacheKey) {
            cache.Put("R::" + cacheKey, "true");
        }

        /// <summary>
        /// Ends the refresh operation of a given cache key.
        /// </summary>
        /// <param name="cache">Cache service.</param>
        /// <param name="cacheKey">Cache key to refresh.</param>
        private static void FinishRefresh(this ICacheService cache, string cacheKey) {
            cache.Remove("R::" + cacheKey);
        }
    }
}