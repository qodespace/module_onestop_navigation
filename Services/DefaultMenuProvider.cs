﻿using System.Linq;
using System.Web;
using Onestop.Navigation.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Aspects;
using Orchard.Core.Navigation.Models;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.Security.Permissions;
using Orchard.UI;
using Orchard.UI.Navigation;

namespace Onestop.Navigation.Services {
    /// <summary>
    /// Overrides the default menu provider. This is the main piece of code responsible for 
    /// fetching data and and preparing it for navigation manager.
    /// </summary>
    [OrchardSuppressDependency("Orchard.Core.Navigation.Services.DefaultMenuProvider")]
    public class DefaultMenuProvider : IMenuProvider {
        private readonly IContentManager _contentManager;
        private readonly IMenuService _menuService;

        public DefaultMenuProvider(IContentManager contentManager, IMenuService menuService) {
            _contentManager = contentManager;
            _menuService = menuService;
        }

        public void GetMenu(IContent menu, NavigationBuilder builder)
        {
            var menuItems = _menuService.GetMenuItems(menu).OrderBy(i => i.As<ExtendedMenuItemPart>().Position, new FlatPositionComparer());

            foreach (var menuPart in menuItems) {
                if (menuPart != null) {
                    var part = menuPart;

                    // fetch the culture of the menu item, if any
                    string culture = null;
                    var localized = part.As<ILocalizableAspect>();
                    if(localized != null) {
                        culture = localized.Culture;
                    }

                    var permission = !string.IsNullOrWhiteSpace(part.As<ExtendedMenuItemPart>().Permission) && !part.As<ExtendedMenuItemPart>().Permission.Equals("<None>")
                                         ? Permission.Named(part.As<ExtendedMenuItemPart>().Permission)
                                         : null;

                    var technicalName = part.As<ExtendedMenuItemPart>().TechnicalName;

                    if (part.Is<MenuItemPart>())
                        builder.Add(
                            new LocalizedString(HttpUtility.HtmlEncode(part.As<ExtendedMenuItemPart>().Text)), 
                            part.As<ExtendedMenuItemPart>().Position,
                            item =>
                                {
                                    item.Url(part.As<MenuItemPart>().Url)
                                        .Content(part)
                                        .Culture(culture)
                                        .IdHint(technicalName);

                                    if (permission != null) item.Permission(permission);
                                });
                    else
                        builder.Add(
                            new LocalizedString(HttpUtility.HtmlEncode(part.As<ExtendedMenuItemPart>().Text)), 
                            part.As<ExtendedMenuItemPart>().Position,
                            item =>
                                {
                                    item.Action(_contentManager.GetItemMetadata(part.ContentItem).DisplayRouteValues)
                                        .Content(part)
                                        .Culture(culture)
                                        .IdHint(technicalName);

                                    if (permission != null) item.Permission(permission);
                                });

                }
            }
        }
    }
}