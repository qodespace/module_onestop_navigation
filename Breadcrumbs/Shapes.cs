﻿using System;
using System.Collections.Generic;
using Onestop.Navigation.Breadcrumbs.Services.Implementations;
using Onestop.Navigation.Services;
using Orchard.Caching;
using Orchard.Caching.Services;
using Orchard.ContentManagement;
using Orchard.DisplayManagement.Descriptors;
using Orchard.DisplayManagement.Implementation;
using Orchard.Environment.Extensions;
using Orchard.Utility.Extensions;
using Orchard.Widgets.Models;

namespace Onestop.Navigation.Breadcrumbs
{
    [OrchardFeature("Onestop.Navigation.Breadcrumbs")]
    public class Shapes : IShapeTableProvider
    {
        private readonly ICacheService _cache;

        public Shapes(ICacheService cache)
        {
            _cache = cache;
        }

        public void Discover(ShapeTableBuilder builder) {
            builder.Describe("Breadcrumbs")
                .OnDisplaying(displaying =>
                    {
                        var shape = displaying.Shape;
                        var crumbs = shape.LazyBreadcrumbs.Value as Services.Breadcrumbs;

                        // Rewriting the property back to Breadcrumbs for compatibility with existing alternates
                        shape.Breadcrumbs = crumbs;

                        if (crumbs != null) {
                            shape.Metadata.Alternates.Add("Breadcrumbs__" + Encode(crumbs.Context.Provider));

                            if (crumbs.Context.Content != null) {
                                shape.Metadata.Alternates.Add("Breadcrumbs__" + crumbs.Context.Content.Id);
                            }

                            // Breadcrumbs shape is a mutated widget shape
                            ContentItem contentItem = shape.ContentItem;
                            if (contentItem != null && contentItem.Is<WidgetPart>())
                            {
                                var widgetPart = contentItem.As<WidgetPart>();
                                var zoneName = widgetPart.Zone;
                                var layerName = widgetPart.LayerPart.Name;

                                displaying.ShapeMetadata.Alternates.Add("Breadcrumbs__zone__" + zoneName);
                                displaying.ShapeMetadata.Alternates.Add("Breadcrumbs__layer__" + layerName);

                                // using the technical name to add an alternate
                                if (!String.IsNullOrWhiteSpace(widgetPart.Name))
                                {
                                    displaying.ShapeMetadata.Alternates.Add("Breadcrumbs__named__" + widgetPart.Name);
                                }

                            }

                            // Adding alternates based on pattern match named groups
                            object groups;
                            if (crumbs.Context.Properties.TryGetValue("Groups", out groups))
                            {
                                foreach (var group in (IDictionary<string, object>)groups) {
                                    shape.Metadata.Alternates.Add("Breadcrumbs__group__" + group.Key);
                                    shape.Metadata.Alternates.Add("Breadcrumbs__group__" + group.Key + "__" + group.ToString().HtmlClassify().ToSafeName());
                                }
                            }
                        }
                    }).OnDisplayed(OnDisplayed);
        }

        private void OnDisplayed(ShapeDisplayedContext displayed)
        {
            string key = displayed.Shape.PleaseCacheKey;
            if (key == null) return;

            int version = displayed.Shape.PleaseCacheVersion;

            var value = displayed.ChildContent;
            if (value != null) {
                _cache.PutVersion(key, value.ToString(), version);
            }
        }

        private string Encode(string alternateElement)
        {
            return alternateElement.Replace("-", "__").Replace(".", "_");
        }
    }
}