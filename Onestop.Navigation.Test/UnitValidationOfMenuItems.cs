﻿using System;
using System.Net.Http;
using Autofac;
using NUnit.Framework;
using Orchard.Caching;

namespace Onestop.Navigation.Tests
{
    [TestFixture]
    public class OnestopNavigationTests {
        private IContainer _container;
        private ICacheManager _cacheManager;

        [SetUp]
        public void Init() {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CacheModule());
            builder.RegisterType<DefaultCacheManager>().As<ICacheManager>();
            builder.RegisterType<DefaultCacheHolder>().As<ICacheHolder>().SingleInstance();
            builder.RegisterType<DefaultCacheContextAccessor>().As<ICacheContextAccessor>();
            _container = builder.Build();
            _cacheManager = _container.Resolve<ICacheManager>(new TypedParameter(typeof(Type), GetType()));
        }

        [Test]
        public void NavigationShouldReturnDataFromCache()
        {
            //var client = new HttpClient();
            //var uri = new Uri("http://varv.local.onestop.com");
            //var response = client.GetAsync(uri).Result;

            //var result = _cacheManager.Get("Shape::OnestopMenuWidget::Header_610_AllItems_0__False_False__en-US___", ctx => "testResult");
            //Assert.That(result, !Is.EqualTo("testResult"));
        }
    }
}
