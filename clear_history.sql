/*
   Script to clear history records in Onestop.Navigation.  It leaves the current version intact.
   Search/Replace Change the prefix_ for any client.
*/

DECLARE @ItemIdsToDelete TABLE
(
	Id int
)

DECLARE @ItemVersionIdsToKeep TABLE
(
	Id int,
	ItemId int
)

DECLARE @ItemVersionIdsToDelete TABLE
(
	Id int
)

DECLARE @MenuVersionIdsToDelete TABLE
(
	Id int
)

DECLARE @MenuTypeId int
SELECT @MenuTypeId = Id FROM blnd_Orchard_Framework_ContentTypeRecord WHERE Name = 'Menu'

INSERT INTO @ItemVersionIdsToKeep(Id, ItemId)
SELECT V.Id, V.ContentItemRecord_id
FROM 
	blnd_Onestop_Navigation_ExtendedMenuItemPartRecord MI INNER JOIN
	blnd_Orchard_Framework_ContentItemVersionRecord V ON MI.Id = V.ID
WHERE V.Published = 1 OR V.Latest = 1


INSERT INTO @ItemVersionIdsToDelete(Id)
SELECT DISTINCT M.Id
FROM blnd_Onestop_Navigation_ExtendedMenuItemPartRecord M 
WHERE M.Id NOT IN (SELECT Id FROM @ItemVersionIdsToKeep)


INSERT INTO @ItemIdsToDelete(Id)
SELECT DISTINCT M.ContentItemRecord_id
FROM blnd_Onestop_Navigation_ExtendedMenuItemPartRecord M 
WHERE M.ContentItemRecord_id NOT IN (SELECT ItemId FROM @ItemVersionIdsToKeep)


INSERT INTO @MenuVersionIdsToDelete(Id)
SELECT DISTINCT V.Id
FROM         
    blnd_Orchard_Framework_ContentItemRecord C INNER JOIN 	
    blnd_Orchard_Framework_ContentItemVersionRecord V ON C.Id = V.ContentItemRecord_id 
WHERE C.ContentType_id = @MenuTypeId AND V.Published = 0 AND V.Latest = 0

-- Removing historical menu item versions
DELETE FROM blnd_Onestop_Navigation_ExtendedMenuItemPartRecord WHERE Id IN (SELECT Id FROM @ItemVersionIdsToDelete) OR ContentItemRecord_id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Onestop_Navigation_VersionInfoPartRecord WHERE Id IN (SELECT Id FROM @ItemVersionIdsToDelete) OR ContentItemRecord_id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Orchard_Framework_ContentItemVersionRecord WHERE Id IN (SELECT Id FROM @ItemVersionIdsToDelete) OR ContentItemRecord_id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Orchard_Framework_ContentItemRecord WHERE Id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Common_CommonPartVersionRecord WHERE Id IN (SELECT Id FROM @ItemVersionIdsToDelete) OR ContentItemRecord_id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Common_CommonPartRecord WHERE Id IN (SELECT Id FROM @ItemIdsToDelete)

DELETE FROM blnd_Common_IdentityPartRecord WHERE Id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Title_TitlePartRecord WHERE Id IN (SELECT Id FROM @ItemVersionIdsToDelete) OR ContentItemRecord_id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Common_BodyPartRecord WHERE Id IN (SELECT Id FROM @ItemVersionIdsToDelete) OR ContentItemRecord_id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Navigation_MenuPartRecord WHERE Id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Navigation_MenuItemPartRecord WHERE Id IN (SELECT Id FROM @ItemIdsToDelete)
DELETE FROM blnd_Navigation_ContentMenuItemPartRecord WHERE Id IN (SELECT Id FROM @ItemIdsToDelete)

-- Removing historical menu versions
DELETE FROM blnd_Orchard_Framework_ContentItemVersionRecord WHERE Id IN (SELECT Id FROM @MenuVersionIdsToDelete)
DELETE FROM blnd_Title_TitlePartRecord WHERE Id IN (SELECT Id FROM @MenuVersionIdsToDelete)
DELETE FROM blnd_Common_CommonPartVersionRecord WHERE Id IN (SELECT Id FROM @MenuVersionIdsToDelete)
DELETE FROM blnd_Onestop_Navigation_VersionInfoPartRecord WHERE Id IN (SELECT Id FROM @MenuVersionIdsToDelete)
