﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Tasks.Indexing;

namespace Onestop.Navigation.Handlers.Overrides {
    /// <summary>
    /// Intercepts the ContentHandler events to create indexing tasks when a content item 
    /// is published, and to delete them when the content item is unpublished.
    /// </summary>
    [OrchardSuppressDependency("Orchard.Indexing.Handlers.CreateIndexingTaskHandler")]
    public class CreateIndexingTaskHandler : ContentHandler {
        private readonly IIndexingTaskManager _indexingTaskManager;

        public CreateIndexingTaskHandler(IIndexingTaskManager indexingTaskManager) {
            _indexingTaskManager = indexingTaskManager;

            OnPublished<ContentPart>(CreateIndexingTask);
            OnUnpublished<ContentPart>(CreateIndexingTask);
            OnRemoved<ContentPart>(RemoveIndexingTask);
        }

        void CreateIndexingTask(PublishContentContext context, ContentPart part) {
            if (HasMenuItemStereotype(part)) return;

            // "Unpublish" case: Same as "remove"
            if (context.PublishingItemVersionRecord == null) {
                _indexingTaskManager.CreateDeleteIndexTask(context.ContentItem);
                return;
            }
            // "Publish" case: update index
            _indexingTaskManager.CreateUpdateIndexTask(context.ContentItem);
        }

        void RemoveIndexingTask(RemoveContentContext context, ContentPart part) {
            if (HasMenuItemStereotype(part)) return;
            _indexingTaskManager.CreateDeleteIndexTask(context.ContentItem);
        }

        private static bool HasMenuItemStereotype(IContent part)
        {
            return part.ContentItem.TypeDefinition.Settings.ContainsKey("Stereotype") &&
                   part.ContentItem.TypeDefinition.Settings["Stereotype"] == "MenuItem";
        }
    }
}
