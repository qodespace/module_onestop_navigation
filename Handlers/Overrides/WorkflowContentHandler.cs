﻿using System;
using System.Collections.Generic;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Events;

namespace Onestop.Navigation.Handlers.Overrides {
    public interface IWorkflowManager : IEventHandler
    {
        void TriggerEvent(string name, IContent target, Func<Dictionary<string, object>> tokensContext);
    }

    [OrchardSuppressDependency("Orchard.Workflows.Handlers.WorkflowContentHandler")]
    public class WorkflowContentHandler : ContentHandler {
        private readonly IWorkflowManager _workflowManager;

        public WorkflowContentHandler(IWorkflowManager workflowManager) {
            _workflowManager = workflowManager;

            OnPublished<ContentPart>(
                (context, part) => 
                    TriggerEvent("ContentPublished", 
                    context.ContentItem, 
                    () => new Dictionary<string, object> { { "Content", context.ContentItem } }));

            OnRemoving<ContentPart>(
                (context, part) =>
                    TriggerEvent("ContentRemoved", 
                    context.ContentItem, 
                    () => new Dictionary<string, object> { { "Content", context.ContentItem } }));

            OnVersioned<ContentPart>(
                (context, part1, part2) =>
                    TriggerEvent("ContentVersioned",
                    context.BuildingContentItem,
                    () => new Dictionary<string, object> { { "Content", context.BuildingContentItem } }));

            OnCreated<ContentPart>(
                (context, part) =>
                    TriggerEvent("ContentCreated", context.ContentItem,
                    () => new Dictionary<string, object> { { "Content", context.ContentItem } }));

            OnUpdated<ContentPart>(
                (context, part) =>
                    TriggerEvent("ContentUpdated", context.ContentItem,
                    () => new Dictionary<string, object> { { "Content", context.ContentItem } }));
        }

        private void TriggerEvent(string name, IContent target, Func<Dictionary<string, object>> tokensContext) {
            if (HasMenuItemStereotype(target)) return;
            _workflowManager.TriggerEvent(name, target, tokensContext);
        }

        private static bool HasMenuItemStereotype(IContent part)
        {
            return part.ContentItem.TypeDefinition.Settings.ContainsKey("Stereotype") &&
                   part.ContentItem.TypeDefinition.Settings["Stereotype"] == "MenuItem";
        }
    }
}