﻿using System;
using System.Collections.Generic;
using Onestop.Navigation.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Events;

namespace Onestop.Navigation.Handlers.Overrides
{
    public interface IRulesManager : IEventHandler
    {
        void TriggerEvent(string category, string type, Func<Dictionary<string, object>> tokensContext);
        void ExecuteActions(dynamic actions, Dictionary<string, object> tokens);
    }

    [OrchardSuppressDependency("Orchard.Rules.Handlers.RulePartHandler")]
    public class RulePartHandler : ContentHandler {
        private readonly IRulesManager _rulesManager;

        public RulePartHandler(IRulesManager rulesManager) {
            _rulesManager = rulesManager;

            OnPublished<ContentPart>((context, part) => 
                TriggerEvent("Content", "Published", part, () => new Dictionary<string, object> { { "Content", context.ContentItem } }));

            OnRemoved<ContentPart>((context, part) => 
                TriggerEvent("Content", "Removed", part, () => new Dictionary<string, object> { { "Content", context.ContentItem } }));

            OnVersioned<ContentPart>((context, part1, part2) => 
                TriggerEvent("Content", "Versioned", part1, () => new Dictionary<string, object> { { "Content", part1.ContentItem } }));

            OnCreated<ContentPart>((context, part) => 
                TriggerEvent("Content", "Created", part, () => new Dictionary<string, object> { { "Content", context.ContentItem } }));
        }

        private void TriggerEvent(string category, string type,  IContent target, Func<Dictionary<string, object>> tokensContext)
        {
            if (HasMenuItemStereotype(target)) return;
            _rulesManager.TriggerEvent(category, type, tokensContext);
        }

        private static bool HasMenuItemStereotype(IContent part)
        {
            return part.ContentItem.TypeDefinition.Settings.ContainsKey("Stereotype") &&
                   part.ContentItem.TypeDefinition.Settings["Stereotype"] == "MenuItem";
        }
    }
}